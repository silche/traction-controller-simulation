% traction model script

% slip = differences in speeds divided by 
% the tolerance value plus the velocity

% motor. c23 series specification
L = 0.003; % inductance (henry)
R = 0.141; % resistance (ohm)
% motor constants for size and back emf
Kb = 0.00574; % (Volt * s/rad)
Km = 0.00574; % (N * m/Amp)
J = 0.0001; % moment of inertia (kg * m)
b = 3.97e-6; % viscous friction coefficient, damping factor
fc = 0.0; % friction factor

% car/traction parameters
m = 1.136; % weight, total car mass (kg)
Rw = 3.2e-2; % wheel radius size (m)
g = 9.81; % force of g
Q = 0.37; % weight balance factor (37%)
GR = 2.5; % gear ratio bettween back and front

% initial conditions
i_0 = 0; % current
wm_0 = 0; % motor angular velocity/speed
tht_0 = 0; % angular position
v_0 = 0; % velocity/speed
xp_0 = 0; % position of x

% tolerance
tol = 1.0e-10; % you can put other value, smth like 1.0e-6

% desired speed (speed up to)
wm_des = 275;

% desired speed (slow down to)
wm_des2 = 110;

% input voltage
u1 = 12; % 12v - constant
