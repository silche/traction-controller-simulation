# Speed and Traction Controller for a vehicle powered by a DC motor



## Description

The model allows to simulate the DC motor, wheel friction and slip, and the traction control system to eliminate slippage.

- Simulink Dynamic Modeling (Equations of Motion)
- Two Loop Control System (PID)
- Modeling a DC motor
- Modeling vehicle friction (contact with the ground)

<div align="center">
<img alt="Main view" src="https://gitlab.com/silche/traction-controller-simulation/-/raw/media/screenshots/model/simulink_schemes/speed_traction_controller.png"
    title="Speed traction controller" width="70%">
<p><em>Speed traction controller</em></p>
</div>

<details><summary>DC motor and wheel operation model</summary>
<div align="center">
<img alt="DC motor and wheels operation model" src="https://gitlab.com/silche/traction-controller-simulation/-/raw/media/screenshots/model/simulink_schemes/dc_motor_wheels.png"
    title="DC motor and wheels operation model" width="70%">
<p><em>DC motor and wheels operation model</em></p>
</div>
</details>

<details><summary>Initial parameters</summary>
<div align="center">
<img alt="Initial parameters" src="https://gitlab.com/silche/traction-controller-simulation/-/raw/media/screenshots/model/motor_and_traction_values.png"
    title="Initial parameters" width="30%">
<p align="center"><em>Initial parameters</em></p>
</div>
</details>

## Simulation results

<div align="center">
<img alt="Wheel speeds" src="https://gitlab.com/silche/traction-controller-simulation/-/raw/media/screenshots/wheel_speeds/main.png"
    title="Wheel speeds" width="50%">
<p><em>Wheel speeds</em></p>
<img alt="Slip ratio" src="https://gitlab.com/silche/traction-controller-simulation/-/raw/media/screenshots/slip_ratio.png"
    title="Slip ratio" width="50%">
<p align="center"><em>Slip ratio</em></p>
</div>

<details><summary>Scaled wheel speeds</summary>
<div align="center">
<img alt="Scaled to 0.05" src="https://gitlab.com/silche/traction-controller-simulation/-/raw/media/screenshots/wheel_speeds/scaled_2.png"
    title="Scaled to 0.05" width="38%">
<img alt="Scaled to 5.5" src="https://gitlab.com/silche/traction-controller-simulation/-/raw/media/screenshots/wheel_speeds/scaled_1.png"
    title="Scaled to 5.5" width="42%">
<p align="center"><em>Scaled to 0.05 and 5.5 on the x-axis (time), respectively</em></p>
</div>
Created based on the [tutorial](https://youtu.be/kJ9cGLXBbLw)
</details>

